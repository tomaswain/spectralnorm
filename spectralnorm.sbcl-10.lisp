;;    The Computer Language Benchmarks Game
;;    https://salsa.debian.org/benchmarksgame-team/benchmarksgame/
;;
;;    Adapted from the C (gcc) code by Sebastien Loisel
;;
;;    Contributed by Christopher Neufeld
;;    Modified by Juho Snellman 2005-10-26
;;      * Use SIMPLE-ARRAY instead of ARRAY in declarations
;;      * Use TRUNCATE instead of / for fixnum division
;;      * Rearrange EVAL-A to make it more readable and a bit faster
;;    Modified by Andy Hefner 2008-09-18
;;      * Eliminate array consing
;;      * Clean up type declarations in eval-A
;;      * Distribute work across multiple cores on SBCL
;;    Modified by Witali Kusnezow 2008-12-02
;;      * use right shift instead of truncate for division in eval-A
;;      * redefine eval-A as a macro
;;    Modified by Tomas Wain - 20/11/2020
;;      * Substantial speedup compared to sbcl-9 of Shubhamkar Ayare
;;      * Using SSE calculation in two lanes
;;      * Improvement in type declarations

(declaim (optimize (speed 3) (safety 0) (space 0) (debug 0)))
(sb-int:set-floating-point-modes :traps (list :divide-by-zero))
(declaim (sb-ext:muffle-conditions style-warning))

(in-package :sb-vm)
(eval-when (:compile-toplevel :load-toplevel :execute)
  (deftype %d2 () '(simd-pack double-float))
  (defmacro the! (&rest args) `(sb-ext:truly-the ,@args))
  (declaim (inline %d2+ %d2/ %d2ref %d2set %make-sse-double))
  (defmacro define-double-binary-vop-operation (name sse-operation)
    `(eval-when (:compile-toplevel :load-toplevel :execute)
       (defknown (,name) (%d2 %d2) %d2 (movable flushable always-translatable)
         :overwrite-fndb-silently t)
       (define-vop (,name)
         (:translate ,name)
         (:policy :fast-safe)
         (:args (a :scs (double-sse-reg) :target dest)
                (b :scs (double-sse-reg) :to :save))
         (:arg-types simd-pack-double simd-pack-double)
         (:results (dest :scs (double-sse-reg) :from (:argument 0)))
         (:result-types simd-pack-double)
         (:generator 4 (move dest a)
                       (inst ,sse-operation dest b)))))
  (define-double-binary-vop-operation %d2+ addpd)
  (define-double-binary-vop-operation %d2/ divpd)

  (defknown %d2ref ((simple-array double-float)
                    (integer 0 #.most-positive-fixnum)) %d2
      (movable foldable flushable always-translatable)
    :overwrite-fndb-silently t)
  (define-vop (%d2ref)
    (:translate %d2ref)
    (:args (v :scs (descriptor-reg))
           (i :scs (any-reg)))
    (:arg-types simple-array-double-float
                tagged-num)
    (:results (dest :scs (double-sse-reg)))
    (:result-types simd-pack-double)
    (:policy :fast-safe)
    (:generator 4 (inst movapd dest (float-ref-ea v i 0 8 :scale 4))))

  (defknown %d2set ((simple-array double-float)
                    (integer 0 #.most-positive-fixnum) %d2) %d2
      (always-translatable)
    :overwrite-fndb-silently t)
  (define-vop (%d2set)
    (:translate %d2set)
    (:args (v :scs (descriptor-reg))
           (i :scs (any-reg))
           (x :scs (double-sse-reg) :target dest))
    (:arg-types simple-array-double-float
                tagged-num
                simd-pack-double)
    (:results (dest :scs (double-sse-reg) :from (:argument 2)))
    (:result-types simd-pack-double)
    (:policy :fast-safe)
    (:generator 4 (inst movapd (float-ref-ea v i 0 8 :scale 4) x)))

  (declaim (ftype (function (double-float double-float) %d2) %make-sse-double))
  (defun %make-sse-double (a b)
    (declare (type double-float a b))
    (the! %d2 (sb-vm::%make-simd-pack-double a b))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(in-package :cl-user)
(eval-when (:compile-toplevel :load-toplevel :execute)
  (deftype index () 'sb-int:index)
  (deftype %d2   () '(simd-pack double-float))
  (deftype %d2+  () '(simd-pack (double-float 0d0)))
  (defmacro the! (&rest args) `(sb-ext:truly-the ,@args))

  (declaim (inline make-sse-double d2+ d2/ d2ref (setf d2ref)))
  (declaim (ftype (function (number &optional number) %d2)
                  make-sse-double))
  (defun make-sse-double (a &optional (b a))
    (cond ((typep a 'double-float) (the! %d2 (sb-vm::%make-sse-double a b)))
          ((typep a 'single-float) (the! %d2 (sb-vm::%make-sse-double
                                              (float a 0d0) (float b 0d0))))
          ((typep a 'integer   ) (the! %d2 (sb-vm::%make-sse-double
                                            (float a 0d0) (float b 0d0))))))

  (defmacro define-constant (name value &optional doc)
    `(defconstant ,name (if (boundp ',name) (symbol-value ',name) ,value)
       ,@(when doc (list doc))))
  (define-constant %0.0d2c (make-sse-double 0.0d0))
  (define-constant %1.0d2c (make-sse-double 1.0d0))

  (declaim (ftype (function (&rest %d2) %d2) d2+))
  (defun d2+ (&rest args)
    (cond ((null args) %0.0d2c)
          ((null (cdr args)) (car args))
          ((null (cddr args)) (the! %d2 (sb-vm::%d2+ (car args) (cadr args))))
          (t (the! %d2 (sb-vm::%d2+ (car args) (apply #'d2+ (cdr args)))))))
  (define-compiler-macro d2+ (&whole whole &rest args &environment env)
    (if (> (sb-c::policy-quality (slot-value env 'sb-c::%policy) 'speed)
           (sb-c::policy-quality (slot-value env 'sb-c::%policy) 'space))
        (let ((args (case (car whole)
                      (apply (nconc (butlast (cdr args))(car (last (cdr args)))))
                      (funcall args)
                      (t args))))
          (cond ((null args) %0.0d2c)
                ((null (cdr args)) (car args))
                ((null (cddr args)) `(the! %d2 (sb-vm::%d2+
                                                ,(car args) ,(cadr args))))
                (t `(the! %d2 (sb-vm::%d2+
                               ,(car args)
                               ,(funcall (compiler-macro-function 'd2+)
			                 `(funcall #'d2+ ,@(cdr args)) env))))))
        whole))

  (declaim (ftype (function (%d2 &rest %d2) %d2) d2/))
  (defun d2/ (arg &rest args)
    (cond ((null args) (the! %d2 (sb-vm::%d2/ %1.0d2c arg)))
          ((null (cdr args)) (the! %d2 (sb-vm::%d2/ arg (car args))))
          (t (the! %d2 (apply #'d2/ (sb-vm::%d2/ arg (car args)) (cdr args))))))
  (define-compiler-macro d2/ (&whole whole arg &rest args &environment env)
    (if (> (sb-c::policy-quality (slot-value env 'sb-c::%policy) 'speed)
           (sb-c::policy-quality (slot-value env 'sb-c::%policy) 'space))
        (cond ((null args) `(the! %d2 (sb-vm::%d2/ ,%1.0d2c ,arg)))
              ((null (cdr args)) `(the! %d2 (sb-vm::%d2/ ,arg ,(car args))))
           (t (funcall (compiler-macro-function 'd2/)
                       `(funcall #'d2/ (the! %d2 (sb-vm::%d2/ ,arg ,(car args))
                                             ,@(cdr args)) ) env)))
        whole))

  (define-modify-macro d2incf (&optional (num %1.0d2c)) d2+)

  (declaim (ftype (function ((simple-array double-float (*)) index) %d2) d2ref))
  (defun d2ref (v i)
    (declare (type (simple-array double-float (*)) v)
             (type index i))
    (the! %d2 (sb-vm::%d2ref v i)))

  (declaim (ftype (function (%d2 (simple-array double-float (*)) index) %d2)
                  (setf d2ref)))
  (defun (setf d2ref) (new-value v i)
    (declare (type index i)
             (type %d2 new-value)
             (type (simple-array double-float (*)) v))
    (the! %d2 (sb-vm::%d2set v i new-value))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(in-package :cl-user)
(deftype int31 (&optional (bits 31)) `(signed-byte ,bits))
(deftype d+array () '(simple-array (double-float 0d0) (*)))

(defmacro Index (i j)
  `(let* ((n (+ ,i ,j))
          (n+1 (1+ n)))
     (float (+ (ash (* n n+1) -1) ,i 1) 0d0)))

(declaim (ftype (function (d+array d+array int31 int31 int31) null) EvalPart0))
(defun EvalPart0 (src dst begin end length)
  (let ((k begin)
	(src0 (aref src 0)))
    (loop for i from begin below end by 4
          do (let* ((I0     (Index (+ i 0) 0)) (I1 (Index (+ i 1) 0))
                    (I2     (Index (+ i 2) 0)) (I3 (Index (+ i 3) 0))
                    (%sum1  (make-sse-double (/ src0 I0) (/ src0 I1)))
                    (%sum2  (make-sse-double (/ src0 I2) (/ src0 I3)))
                    (%ti1   (make-sse-double (+ i 0) (+ i 1)))
                    (%ti2   (make-sse-double (+ i 2) (+ i 3)))
                    (%last1 (make-sse-double I0 I1))
                    (%last2 (make-sse-double I2 I3)))
               (loop for j from 1 below length
                     do (let* ((%j     (make-sse-double j))
		               (%src-j (make-sse-double (aref src j)))
		               (%idx1  (d2+ %last1 %ti1 %j))
		               (%idx2  (d2+ %last2 %ti2 %j)))
	                  (setf %last1 %idx1) (setf %last2 %idx2)
	                  (d2incf %sum1 (d2/ %src-j %idx1))
	                  (d2incf %sum2 (d2/ %src-j %idx2))))
	           (setf (d2ref dst i) %sum1)
	           (setf (d2ref dst (+ i 2)) %sum2))
             (incf k 4))
    (loop with sum of-type double-float = 0d0
	  for i from k below end
          do (loop for j below length
                   do (incf sum (/ (aref src j) (Index i j))))
	     (setf (aref dst i) sum))))

(declaim (ftype (function (d+array d+array int31 int31 int31) null) EvalPart1))
(defun EvalPart1 (src dst begin end length)
  (let ((k begin)
	(src0 (aref src 0)))
    (loop for i from begin below end by 4
          do (let* ((I0     (Index 0 (+ i 0))) (I1 (Index 0 (+ i 1)))
                    (I2     (Index 0 (+ i 2))) (I3 (Index 0 (+ i 3)))
                    (%sum1  (make-sse-double (/ src0 I0) (/ src0 I1)))
                    (%sum2  (make-sse-double (/ src0 I2) (/ src0 I3)))
                    (%ti1   (make-sse-double (+ i 1) (+ i 2)))
                    (%ti2   (make-sse-double (+ i 3) (+ i 4)))
                    (%last1 (make-sse-double I0 I1))
                    (%last2 (make-sse-double I2 I3)))
	       (loop for j from 1 below length
                     do (let* ((%j     (make-sse-double j))
		               (%src-j (make-sse-double (aref src j)))
		               (%idx1  (d2+ %last1 %ti1 %j))
		               (%idx2  (d2+ %last2 %ti2 %j)))
	                  (setf %last1 %idx1) (setf %last2 %idx2)
	                  (d2incf %sum1 (d2/ %src-j %idx1))
	                  (d2incf %sum2 (d2/ %src-j %idx2))))
	       (setf (d2ref dst i) %sum1)
	       (setf (d2ref dst (+ i 2)) %sum2))
             (incf k 4))
    (loop with sum of-type double-float = 0d0
	  for i from k below end
          do (loop for j below length
                   do (incf sum (/ (aref src j) (Index j i))))
	     (setf (aref dst i) sum))))

(declaim (ftype (function () (integer 1 256)) GetThreadCount))
#+sb-thread
(defun GetThreadCount ()
  (progn (define-alien-routine sysconf long (name int))
         (sysconf 84)))

(declaim (ftype (function (int31 int31 function) null)
                execute-parallel execute-serial))
#+sb-thread
(defun execute-parallel (start end function)
  (declare (optimize (speed 0)))
  (let ((step (multiple-value-bind (n _)(truncate (- end start)(GetThreadCount))
		(declare (ignore _)) (- n (mod n 2)))))
    (loop for i from start below end by step
          collecting (let ((start i)
                           (end (min end (+ i step))))
                       (sb-thread:make-thread
			(lambda () (funcall function start end))))
          into threads
          finally (mapcar #'sb-thread:join-thread threads))))

#-sb-thread
(defun execute-parallel (start end function)
  (funcall function start end))

(defun execute-serial (start end function)
  (funcall function start end))

(declaim (ftype (function (d+array d+array int31 int31 int31) null)
                EvalATimesU EvalAtTimesU))
(defun EvalATimesU (src dst start end N)
  (EvalPart1 src dst start end N))

(defun EvalAtTimesU (src dst start end N)
  (EvalPart0 src dst start end N))

(declaim (ftype (function (d+array d+array d+array int31 int31 int31) null)
                EvalAtATimesU))
(defun evalAtATimesU (src dst tmp start end N)
  (if (< n 512) ;; Runs OK if number of virtual threads are < 256
      (progn
	(execute-serial start end (lambda (start end)
				    (EvalATimesU src tmp start end N)))
	(execute-serial start end (lambda (start end)
				    (EvalAtTimesU tmp dst start end N))))
      (progn
	(execute-parallel start end (lambda (start end)
				      (EvalATimesU src tmp start end N)))
	(execute-parallel start end (lambda (start end)
                                      (EvalAtTimesU tmp dst start end N))))))

(declaim (ftype (function (int31) (double-float 0d0)) spectral-game))
(defun spectral-game (N)
  (let ((u (make-array N :element-type 'double-float :initial-element 1.0d0))
        (v (make-array N :element-type 'double-float))
        (tmp (make-array N :element-type 'double-float)))
    (declare (type d+array u v tmp))
    (loop repeat 10 do
          (evalAtATimesU u v tmp 0 N N)
          (evalAtATimesU v u tmp 0 N N))
    (let ((sumvb 0d0)
          (sumvv 0d0))
      (loop for i below N
            with aref-u-i of-type (double-float 0d0) = (aref u i)
            with aref-v-i of-type (double-float 0d0) = (aref v i)
            do (incf sumvb (the! (double-float 0d0) (* aref-u-i aref-v-i)))
               (incf sumvv (the! (double-float 0d0) (* aref-v-i aref-v-i))))
      (sqrt (the! (double-float 0d0) (/ sumvb sumvv))))))

(defun main (&optional n-supplied)
  (let ((N (or n-supplied (parse-integer (or (second sb-ext::*posix-argv*)
                                             "5500")))))
    (or (typep (* (- (* 2 N) 1) (- (* 2 N) 2)) 'fixnum)
        (error "the supplied value of 'n' breaks the optimizations in eval-a"))
    (format t "~11,9F~%" (spectral-game N))))
