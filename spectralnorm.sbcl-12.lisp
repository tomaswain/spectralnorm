;;    The Computer Language Benchmarks Game
;;    https://salsa.debian.org/benchmarksgame-team/benchmarksgame/
;;
;;    Adapted from the C (gcc) code by Sebastien Loisel
;;
;;    Contributed by Christopher Neufeld
;;    Modified by Juho Snellman 2005-10-26
;;      * Use SIMPLE-ARRAY instead of ARRAY in declarations
;;      * Use TRUNCATE instead of / for fixnum division
;;      * Rearrange EVAL-A to make it more readable and a bit faster
;;    Modified by Andy Hefner 2008-09-18
;;      * Eliminate array consing
;;      * Clean up type declarations in eval-A
;;      * Distribute work across multiple cores on SBCL
;;    Modified by Witali Kusnezow 2008-12-02
;;      * use right shift instead of truncate for division in eval-A
;;      * redefine eval-A as a macro
;;    Modified by Tomas Wain
;;      * Substantial speedup compared to sbcl-9 of Shubhamkar Ayare
;;      * Using SSE calculation in two lanes
;;      * Uding sb-simd
;;      * Improvement in type declarations
(declaim (optimize (speed 3) (safety 0) (space 0) (debug 0)))

(ql:quickload :sb-simd :silent t)
(use-package :sb-simd)

(deftype int31 (&optional (bits 31)) `(signed-byte ,bits))
(deftype d+array () '(simple-array (double-float 0d0) (*)))
(deftype d+ () '(double-float 0d0))

(defmacro eval-A (i j)
  `(let* ((n (+ ,i ,j))
          (n+1 (1+ n)))
     (coerce (+ (ash (* n n+1) -1) ,i 1) 'double-float)))

(declaim (ftype (function (d+array d+array int31 int31 int31) null) Eval-A-times-u Eval-At-times-u))
(defun eval-A-times-u (src dst begin end length)
  (loop with src0 of-type double-float = (aref src 0)
	for i from begin below end by 4
	do (let* ((I0     (Eval-A (+ i 0) 0)) (I1 (Eval-A (+ i 1) 0))
		  (I2     (Eval-A (+ i 2) 0)) (I3 (Eval-A (+ i 3) 0))
		  (%sum1  (make-f64.2 (/ src0 I0) (/ src0 I1)))
		  (%sum2  (make-f64.2 (/ src0 I2) (/ src0 I3)))
		  (%ti1   (make-f64.2 (+ i 0) (+ i 1)))
		  (%ti2   (make-f64.2 (+ i 2) (+ i 3)))
		  (%last1 (make-f64.2 I0 I1))
		  (%last2 (make-f64.2 I2 I3)))
	     (loop for j from 1 below length
		   do (let* ((%j     (make-f64.2 j j))
			     (src-j  (aref src j))
			     (%src-j (make-f64.2 src-j src-j))
			     (%idx1  (f64.2+ %last1 %ti1 %j))
			     (%idx2  (f64.2+ %last2 %ti2 %j)))
			(setf %last1 %idx1) (setf %last2 %idx2)
			(f64.2-incf %sum1 (f64.2/ %src-j %idx1))
			(f64.2-incf %sum2 (f64.2/ %src-j %idx2))))
	     (setf (f64.2-ref dst i) %sum1)
	     (setf (f64.2-ref dst (+ i 2)) %sum2))))

(defun Eval-At-times-u (src dst begin end length)
  (loop with src0 of-type double-float = (aref src 0)
	for i from begin below end by 4
        do (let* ((I0     (Eval-A 0 (+ i 0))) (I1 (Eval-A 0 (+ i 1)))
                  (I2     (Eval-A 0 (+ i 2))) (I3 (Eval-A 0 (+ i 3)))
                  (%sum1  (make-f64.2 (/ src0 I0) (/ src0 I1)))
                  (%sum2  (make-f64.2 (/ src0 I2) (/ src0 I3)))
                  (%ti1   (make-f64.2 (+ i 1) (+ i 2)))
                  (%ti2   (make-f64.2 (+ i 3) (+ i 4)))
                  (%last1 (make-f64.2 I0 I1))
                  (%last2 (make-f64.2 I2 I3)))
	     (loop for j from 1 below length
                   do (let* ((%j     (make-f64.2 j j))
			     (src-j  (aref src j))
			     (%src-j (make-f64.2 src-j src-j))
			     (%idx1  (f64.2+ %last1 %ti1 %j))
			     (%idx2  (f64.2+ %last2 %ti2 %j)))
			(setf %last1 %idx1) (setf %last2 %idx2)
			(f64.2-incf %sum1 (f64.2/ %src-j %idx1))
			(f64.2-incf %sum2 (f64.2/ %src-j %idx2))))
	     (setf (f64.2-ref dst i) %sum1)
	     (setf (f64.2-ref dst (+ i 2)) %sum2))))

(declaim (ftype (function () (integer 1 256)) GetThreadCount))
#+sb-thread
(defun get-thread-count ()
  (progn (define-alien-routine sysconf long (name int))
         (sysconf 84)))

(declaim (ftype (function (int31 int31 function) null) execute-parallel))
#+sb-thread
(defun execute-parallel (start end function)
  (declare (optimize (speed 0)))
  (let ((step (multiple-value-bind (n _)(truncate (- end start) (get-thread-count))
		(declare (ignore _)) (- n (mod n 2)))))
    (loop for i from start below end by step
          collecting (let ((start i)
                           (end (min end (+ i step))))
                       (sb-thread:make-thread
			(lambda () (funcall function start end))))
            into threads
          finally (mapcar #'sb-thread:join-thread threads))))

#-sb-thread
(defun execute-parallel (start end function)
  (funcall function start end))

(declaim (ftype (function (d+array d+array d+array int31 int31 int31) null)
                eval-AtA-times-u))
(defun eval-AtA-times-u (src dst tmp start end N)
      (progn
	(execute-parallel start end (lambda (start end)
				      (eval-A-times-u src tmp start end N)))
	(execute-parallel start end (lambda (start end)
				      (eval-At-times-u tmp dst start end N)))))

(declaim (ftype (function (int31) d+) spectralnorm))
(defun spectralnorm (n)
  (let ((u (make-array (+ n 3) :element-type 'd+ :initial-element 1d0))
        (v (make-array (+ n 3) :element-type 'd+))
        (tmp (make-array (+ n 3) :element-type 'd+)))
    (declare (type d+array u v tmp))
    (loop repeat 10 do
      (eval-AtA-times-u u v tmp 0 n n)
      (eval-AtA-times-u v u tmp 0 n n))
    (sqrt (/ (f64.2-vdot u v) (f64.2-vdot v v)))))

(declaim (ftype (function (&optional int31) null) main))
(defun main (&optional n-supplied)
  (let ((n (or n-supplied (parse-integer (or (second sb-ext::*posix-argv*)
                                             "5500")))))
    (declare (type int31 n))
    (if (< n 8)
        (error "The supplied value of 'n' bust be at least 8"))
    (format t "~11,9F~%" (spectralnorm n))))
