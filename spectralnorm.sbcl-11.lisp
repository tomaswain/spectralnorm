;;    The Computer Language Benchmarks Game
;;    https://salsa.debian.org/benchmarksgame-team/benchmarksgame/
;;
;;    Adapted from the C (gcc) code by Sebastien Loisel
;;    Contributed by Christopher Neufeld 2005-08-19
;;    Modified by Juho Snellman 2005-10-26
;;      * Use SIMPLE-ARRAY instead of ARRAY in declarations
;;      * Rearrange EVAL-A to make it more readable and a bit faster
;;    Modified by Andy Hefner 2008-09-18
;;      * Eliminate array consing
;;      * Clean up type declarations in eval-A
;;      * Distribute work across multiple cores on SBCL
;;    Modified by Isaac Gouy 2019-10-21
;;      * eval-A like C gcc #4 program
;;      * posix-argv like Jon Smith's fannkuch-redux Lisp SBCL #2 program
;;      * deftype & type function suggested by tfb on SO
;;    Modified by Tomas Wain 2021-02-22
;;      * removed a multiplication
;;      * changed main

(declaim (optimize (speed 3) (safety 0) (space 0) (debug 0)))

(deftype int31 (&optional (bits 31)) `(signed-byte ,bits))
(deftype d+ () '(double-float 0d0))
(deftype d+array () '(simple-array d+ (*)))

(defmacro eval-A (i j)
  `(let* ((n (+ ,i ,j))
          (n+1 (1+ n)))
     (declare (type int31 n n+1 i j))
     (+ (ash (* n n+1) -1) ,i 1.0d0)))

(declaim (ftype (function (d+array int31 d+array int31 int31) null) eval-At-times-u))
(defun eval-At-times-u (u n Au start end)
  (declare (type int31 n start end)
           (type d+array u Au))
  (loop for i of-type int31 from start below end do
        (setf (aref Au i)
              (loop for j of-type int31 below n
                    summing (/ (aref u j) (eval-A j i))
                    of-type d+))))

(declaim (ftype (function (d+array int31 d+array int31 int31) null) eval-A-times-u))
(defun eval-A-times-u (u n Au start end)
  (declare (type int31 n start end)
           (type d+array u Au))
  (loop for i of-type int31 from start below end do
        (setf (aref Au i)
              (loop for j of-type int31 below n
                    summing (/ (aref u j) (eval-A i j))
                    of-type d+))))

(declaim (ftype (function (int31 int31 function) null) execute-parallel))
#+sb-thread
(defun execute-parallel (start end function)
  (declare (type int31 start end))
  (let* ((num-threads 4))
    (declare (type function function))
    (loop with step = (truncate (- end start) num-threads)
          for index from start below end by step
          collecting (let ((start index)
                           (end (min end (+ index step))))
                       (sb-thread:make-thread
                        (lambda () (funcall function start end))))
          into threads
          finally (mapcar #'sb-thread:join-thread threads))))

#-sb-thread
(defun execute-parallel (start end function)
  (funcall function start end))

(declaim (ftype (function (d+array d+array d+array int31 int31 int31) null) eval-AtA-times-u))
(defun eval-AtA-times-u (u AtAu v n start end)
  (declare (type int31 n start end)
           (type d+array u v AtAu))
  (execute-parallel start end
                       (lambda (start end)
                         (eval-A-times-u u n v start end)))
  (execute-parallel start end
    (lambda (start end)
      (eval-At-times-u v n AtAu start end))))

(declaim (ftype (function (int31) d+) spectralnorm))
(defun spectralnorm (n)
    (let ((u (make-array n :element-type 'd+ :initial-element 1.0d0))
          (v (make-array n :element-type 'd+))
          (tmp (make-array n :element-type 'd+)))
      (declare (type d+array u v tmp))
      (dotimes (i 10)
        (eval-AtA-times-u u v tmp n 0 n)
        (eval-AtA-times-u v u tmp n 0 n))
      (let ((vBv 0.0d0)
            (vv 0.0d0))
       (declare (type d+ vBv vv))
       (dotimes (i n)
         (incf vBv (* (aref u i) (aref v i)))
         (incf vv (* (aref v i) (aref v i))))
       (sqrt (sb-ext:truly-the d+ (/ vBv vv))))))

(defun main (&optional n-supplied)
  (let ((n (or n-supplied (parse-integer (or (car (last #+sbcl sb-ext:*posix-argv*))
                                          "5500")))))
    (declare (type int31 n))
    (or (typep (* (- (* 2 n) 1) (- (* 2 n) 2)) 'fixnum)
       (error "The supplied value of 'n' breaks the optimizations in EVAL-A"))
    (format t "~11,9F~%" (spectralnorm n))))
